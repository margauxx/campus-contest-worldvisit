package com.example.worldvisit

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import java.util.*

class CoursesAdapter(private var listeCourses: MutableList<Course>) :
        RecyclerView.Adapter<CoursesAdapter.CourseViewHolder>()
{
    // Crée chaque vue item à afficher :
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CourseViewHolder
    {
        val viewCourse = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_liste, parent, false)
        return CourseViewHolder(viewCourse)
    }

    // Renseigne le contenu de chaque vue item :
    override fun onBindViewHolder(holder: CourseViewHolder, position: Int)
    {
        holder.textViewLibelleCourse.text = listeCourses[position].intitule
    }

    override fun getItemCount(): Int = listeCourses.size
    // ViewHolder :
    inner class CourseViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
    {
        val textViewLibelleCourse: TextView = itemView.findViewById(R.id.libelle_course)

        init
        {
            textViewLibelleCourse.setOnClickListener {
                val course = listeCourses[adapterPosition]
                Log.d("montag", course.intitule)
            }
        }

    }

    // Appelé à chaque changement de position, pendant un déplacement :
    fun onItemMove(positionDebut: Int, positionFin: Int)
    {
        Collections.swap(listeCourses, positionDebut, positionFin)
        notifyItemMoved(positionDebut, positionFin)
    }


    // Appelé une fois à la suppression :
    fun onItemDismiss(position: Int)
    {
        if (position > -1)
        {
            listeCourses.removeAt(position)
            notifyItemRemoved(position)
        }
    }

    fun onItemDismiss(position: Int, adapterPosition: Int) {
        if (position > -1)
        {
            listeCourses.removeAt(position)
            notifyItemRemoved(position)
        }
    }

}