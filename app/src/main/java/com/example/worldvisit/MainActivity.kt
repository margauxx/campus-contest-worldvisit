package com.example.worldvisit

import android.content.Intent
import android.os.Bundle
import android.preference.PreferenceManager
import android.util.Log
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Button
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat

import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    //private val client = restcountries()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        //LISTES
        // à ajouter pour de meilleures performances :
        liste_courses.setHasFixedSize(true)

        // layout manager, décrivant comment les items sont disposés :
        val layoutManager = LinearLayoutManager(this)
        liste_courses.layoutManager = layoutManager
        // contenu d'exemple :
        val listeCourses: MutableList<Course> = ArrayList()
        listeCourses.add(Course("France"))
        listeCourses.add(Course("Irlande"))

        // adapter :
        val coursesAdapter = CoursesAdapter(listeCourses)
        liste_courses.adapter = coursesAdapter

        //REMOVE
        val itemTouchHelper = ItemTouchHelper(ItemTouchCallback(coursesAdapter))
        itemTouchHelper.attachToRecyclerView(liste_courses)

        val preferences = PreferenceManager.getDefaultSharedPreferences(this)
        val valeur = preferences.getString("1", "")
        if(valeur != ""){
            listeCourses.add(Course(valeur.toString()))
        }

        val maFontBold = ResourcesCompat.getFont(this, R.font.mafont_bold)
        //textView.typeface = maFontBold


    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when(item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    fun clicBoutonCountry(view: View)
    {
        Log.d("montag", "hello")

        val intent = Intent(this, Search::class.java)
        startActivity(intent)
    }

}